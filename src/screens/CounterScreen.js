import React, {useState} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const CounterScreen = () => {
  //Var ไม่เปลี่ยนค่า แต่ใน log เปลี่ยน
  //  var counter = 0;
  const [counter, setCounter] = useState(0);
  return (
    <View>
      <Text>Current Count: {counter}</Text>
      <Button
        title="Increase"
        onPress={() => {
          // Don't do this!
          //   counter++;
          setCounter(counter + 1);
          console.log(counter);
        }}
      />
      <Button
        title="Decrease"
        onPress={() => {
          //Don't do this
          //   counter--;
          setCounter(counter - 1);
          console.log(counter);
        }}
      />
      <Button
        title="Reset"
        onPress={() => {
          //Don't do this
          //   counter--;
          setCounter(0);
          console.log(counter);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({});
export default CounterScreen;

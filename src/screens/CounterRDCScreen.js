import React, {useReducer} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const reducer = (state, action) => {
  //state === {count:number}
  //action === {type='INCREMENT||'DECREMENT', payload: 1 }
  switch (action.type) {
    case 'INCREMENT':
      return {...state, count: state.count + action.payload};
    case 'DECREMENT':
      return {...state, count: state.count - action.payload};
    case 'RESET':
      return {count: 0};
    default:
      break;
  }
};
const CounterRDCScreen = () => {
  const [state, dispatch] = useReducer(reducer, {count: 0});
  return (
    <View>
      <Text>Current Count: {state.count}</Text>
      <Button
        title="Increase"
        onPress={() => {
          dispatch({type: 'INCREMENT', payload: 1});
        }}
      />
      <Button
        title="Decrease"
        onPress={() => {
          dispatch({type: 'DECREMENT', payload: 1});
        }}
      />
      <Button
        title="Reset"
        onPress={() => {
          dispatch({type: 'RESET'});
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({});
export default CounterRDCScreen;

import React, {useReducer} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ColorCounter from '../component/ColorCounter';

const reducer = (state, action) => {
  // state === {red:number,green:number,blue:number}
  // action === {colorTochange:'red' || 'green' || 'blue',amount '15'||'-15'}
  // (state,action) ==> (type,payload) ex.{type:'change_red',payload:15}
  // ==> action === {type:'change_red' || 'change_green' || 'change_blue',amount '15'||'-15'}
  switch (action.colorTochange) {
    case 'red':
      return state.red + action.amount > 255 || state.red + action.amount < 0
        ? state
        : {...state, red: state.red + action.amount};
    //   if (state.red + action.amount > 255 || state.red + action.amount < 0) {
    //     return state;
    //   }
    //   return {...state, red: state.red + action.amount};
    case 'blue':
      return state.blue + action.amount > 255 || state.blue + action.amount < 0
        ? state
        : {...state, blue: state.blue + action.amount};
    case 'green':
      return state.green + action.amount > 255 ||
        state.green + action.amount < 0
        ? state
        : {...state, green: state.green + action.amount};
    default:
      return state;
  }
};
const SquareRDCScreen = () => {
  const COLOR_INCREMENT = 20;
  const [state, dispatch] = useReducer(reducer, {red: 0, green: 0, blue: 0});

  return (
    <View>
      <ColorCounter
        onIncrease={() =>
          dispatch({colorTochange: 'red', amount: COLOR_INCREMENT})
        }
        onDecrease={() =>
          dispatch({colorTochange: 'red', amount: -1 * COLOR_INCREMENT})
        }
        color="Red"
      />
      <ColorCounter
        onIncrease={() =>
          dispatch({colorTochange: 'blue', amount: COLOR_INCREMENT})
        }
        onDecrease={() =>
          dispatch({colorTochange: 'blue', amount: -1 * COLOR_INCREMENT})
        }
        color="Blue"
      />
      <ColorCounter
        onIncrease={() =>
          dispatch({colorTochange: 'green', amount: COLOR_INCREMENT})
        }
        onDecrease={() =>
          dispatch({colorTochange: 'green', amount: -1 * COLOR_INCREMENT})
        }
        color="Green"
      />
      <View
        style={[
          styles.box,
          {backgroundColor: `rgb(${state.red},${state.green},${state.blue})`},
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    height: 150,
    width: 150,
  },
});
export default SquareRDCScreen;

import React, {useState} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import CounterScreen from './CounterScreen';
import {FlatList} from 'react-native-gesture-handler';

const ColorScreen = () => {
  const [colors, setColors] = useState([]);
  return (
    <View>
      <Button
        title="Add color"
        onPress={() => {
          setColors([...colors, randomRGB()]);
        }}
      />
      <FlatList
        keyExtractor={item => item}
        data={colors}
        renderItem={({item}) => {
          return <View style={[styles.box, {backgroundColor: item}]} />;
        }}
      />
    </View>
  );
};
const randomRGB = () => {
  const red = Math.floor(Math.random() * 256);
  const green = Math.floor(Math.random() * 256);
  const blue = Math.floor(Math.random() * 256);
  return `rgb(${red},${green}, ${blue})`;
};
const styles = StyleSheet.create({
  box: {
    height: 100,
    width: 100,
  },
});
export default ColorScreen;

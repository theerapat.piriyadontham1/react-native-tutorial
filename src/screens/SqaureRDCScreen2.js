import React, {useReducer} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ColorCounter from '../component/ColorCounter';

const reducer = (state, action) => {
  // state === {red:number,green:number,blue:number}
  //Changes
  // (state,action) ==> (type,payload) ex.{type:'change_red',payload:15}
  // ==> action === {type:'change_red' || 'change_green' || 'change_blue',amount '15'||'-15'}
  switch (action.type) {
    case 'change_red':
      return state.red + action.payload > 255 || state.red + action.payload < 0
        ? state
        : {...state, red: state.red + action.payload};
    case 'change_blue':
      return state.blue + action.payload > 255 ||
        state.blue + action.payload < 0
        ? state
        : {...state, blue: state.blue + action.payload};
    case 'change_green':
      return state.green + action.amount > 255 ||
        state.green + action.payload < 0
        ? state
        : {...state, green: state.green + action.payload};
    default:
      return state;
  }
};
const SquareRDCScreen2 = () => {
  const COLOR_INCREMENT = 20;
  const [state, dispatch] = useReducer(reducer, {red: 0, green: 0, blue: 0});

  return (
    <View>
      <ColorCounter
        onIncrease={() =>
          dispatch({type: 'change_red', payload: COLOR_INCREMENT})
        }
        onDecrease={() =>
          dispatch({type: 'change_red', payload: -1 * COLOR_INCREMENT})
        }
        color="Red"
      />
      <ColorCounter
        onIncrease={() =>
          dispatch({type: 'change_blue', payload: COLOR_INCREMENT})
        }
        onDecrease={() =>
          dispatch({type: 'change_blue', payload: -1 * COLOR_INCREMENT})
        }
        color="Blue"
      />
      <ColorCounter
        onIncrease={() =>
          dispatch({type: 'change_green', payload: COLOR_INCREMENT})
        }
        onDecrease={() =>
          dispatch({type: 'change_green', payload: -1 * COLOR_INCREMENT})
        }
        color="Green"
      />
      <View
        style={[
          styles.box,
          {backgroundColor: `rgb(${state.red},${state.green},${state.blue})`},
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    height: 150,
    width: 150,
  },
});
export default SquareRDCScreen2;

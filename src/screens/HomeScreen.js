import React from 'react';
import {Text, StyleSheet, View, Button} from 'react-native';

const HomeScreen = props => {
  return (
    <View styles={styles.Container}>
      <Text style={styles.textStyle}>Hello, It's a Homepage</Text>
      <Button
        title="Go to Component"
        onPress={() => props.navigation.navigate('ComponentScreen')}
      />
      <Button
        title="Go to List"
        onPress={() => props.navigation.navigate('ListScreen')}
      />
      <Button
        title="Go to Image demo"
        onPress={() => props.navigation.navigate('ImageScreen')}
      />
      <Button
        title="Go to Counter demo"
        onPress={() => props.navigation.navigate('CounterScreen')}
      />
      <Button
        title="Go to Color demo"
        onPress={() => props.navigation.navigate('ColorScreen')}
      />
      <Button
        title="Go to Square demo"
        onPress={() => props.navigation.navigate('SquareScreen')}
      />
      <Button
        title="Go to Square Reducer demo"
        onPress={() => props.navigation.navigate('SquareRDCScreen')}
      />
      <Button
        title="Go to Square Reducer V.2 demo"
        onPress={() => props.navigation.navigate('SquareRDCScreen')}
      />
      <Button
        title="Go to Counter RDC demo"
        onPress={() => props.navigation.navigate('CounterRDCScreen')}
      />
      <Button
        title="Go to Text Screen"
        onPress={() => props.navigation.navigate('TextScreen')}
      />
      <Button
        title="Go to Box Screen"
        onPress={() => props.navigation.navigate('BoxScreen')}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  textStyle: {
    fontSize: 30,
  },
  Container: {
    alignItems: 'center',
    flexDirection: 'column',
  },
});
export default HomeScreen;

import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const ImageDetail = ({imageSource, title, imageScore}) => {
  //   console.log(props);
  return (
    <View>
      <Image source={imageSource} />
      <Text>{title}</Text>
      <Text>Image score - {imageScore}</Text>
    </View>
  );
};

//Props Version
// const ImageDetail = props => {
//     console.log(props);
//     return (
//       <View>
//         <Image source={props.imageSource} />
//         <Text>{props.title}</Text>
//         <Text>Image score - {props.imageScore}</Text>
//       </View>
//     );
//   };

const styles = StyleSheet.create({});

export default ImageDetail;

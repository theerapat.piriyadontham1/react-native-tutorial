import React from 'react';
// import {SafeAreaView} from 'react-native';
import HomeScreen from './src/screens/HomeScreen';
import ComponentScreen from './src/screens/ComponentScreen';
import ListScreen from './src/screens/ListScreen';
import ImageScreen from './src/screens/ImageScreen';
import CounterScreen from './src/screens/CounterScreen';
import CounterRDCScreen from './src/screens/CounterRDCScreen';
import ColorScreen from './src/screens/ColorScreen';
import SquareScreen from './src/screens/SquareScreen';
import SquareRDCScreen from './src/screens/SquareRDCScreen';
import SquareRDCScreen2 from './src/screens/SqaureRDCScreen2';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import TextScreen from './src/screens/TextScreen';
import BoxScreen from './src/screens/BoxScreen';
const App = () => {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="ListScreen" component={ListScreen} />
        <Stack.Screen name="ComponentScreen" component={ComponentScreen} />
        <Stack.Screen name="ImageScreen" component={ImageScreen} />
        <Stack.Screen name="CounterScreen" component={CounterScreen} />
        <Stack.Screen name="CounterRDCScreen" component={CounterRDCScreen} />
        <Stack.Screen name="ColorScreen" component={ColorScreen} />
        <Stack.Screen name="SquareScreen" component={SquareScreen} />
        <Stack.Screen name="SquareRDCScreen" component={SquareRDCScreen} />
        <Stack.Screen name="SquareRDCScreen2" component={SquareRDCScreen2} />
        <Stack.Screen name="TextScreen" component={TextScreen} />
        <Stack.Screen name="BoxScreen" component={BoxScreen} />
      </Stack.Navigator>
    </NavigationContainer>

    // Ez version
    // <SafeAreaView>
    //   <ListScreen />
    // </SafeAreaView>
  );
};

export default App;
